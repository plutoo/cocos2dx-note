# ios thread

在C++中创建的线程

```
std::thread *loopThread = new std::thread([](){
    while(1) {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        //do log
    }
});
```
在应用切入后台后， 直接`挂起`， 不再执行。

从后台切换到前台会继续执行。 

## connection 

由于`websocket`在独立的线程中loop every 3ms， app从后台切出的时候，还没有改变readyState。在主线城中查询状态可能是错的。 
> 这是一个多线程状态（标记）同步的问题。 

