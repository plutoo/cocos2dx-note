# TODO: Websocket 关于锁的重写？

1. 先看WebSocket.cpp源码
2. 尝试重现bug
    - 死锁
    - 没有onClose回调
3. 升级libwebsockets， 看是否解决问题
4. 重新编译所有平台的libwebsockets



## Xcode

- Xcode提示文件缺失， 选中文件， 在右侧的TargetMemebership里面，添加Target。

## websocket

- WebSocket.cpp/delegate的回调是在网络线程里面执行？

- close 并没有同步？ 可能死锁？ 如果在同一个线程里面wait/notify
