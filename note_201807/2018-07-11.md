# fix libcurl https error

cocos2d-x-lite/CCDownload对https协议的url会产生超时

```diff
-            static const long LOW_SPEED_TIME = 5;
+            static const long LOW_SPEED_TIME = 10;
             curl_easy_setopt(handle, CURLOPT_LOW_SPEED_LIMIT, LOW_SPEED_LIMIT);
             curl_easy_setopt(handle, CURLOPT_LOW_SPEED_TIME, LOW_SPEED_TIME);
 
-            static const int MAX_REDIRS = 2;
+            curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, false);
+            curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, false);
+
+            static const int MAX_REDIRS = 5;
             if (MAX_REDIRS)
             {
                 curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, true);
@@ -507,7 +510,7 @@ namespace cocos2d { namespace network {
                     // do wait action
                     if(maxfd == -1)
                     {
-                        this_thread::sleep_for(chrono::milliseconds(timeoutMS));
+                        this_thread::sleep_for(chrono::milliseconds(50));
                         rc = 0;
                     }
                     else
```

线程中有两个时间

1. `select` timeout
2. `curl_multi_setfd() .. sleep_for`

混淆后后导致超时

## WebSocket实现遇到扯淡的问题。

> HTTP协议的换行符是`\r\n` , 不是 `\n\r` .... 