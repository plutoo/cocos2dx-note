# downloader

## lua静态链接问题

- 平台 `WSL` 和 `mac`
- lua-5.1.5

步骤
1. `cd lua-5.1.5  && make linux`
2. `gcc -o test main.cpp liblua.a -g -std=c++11`

出现所有`lua*`符号找不到的错误。 

原因

```cpp
extern "C" 
{
    #include <lua.h>
}
```