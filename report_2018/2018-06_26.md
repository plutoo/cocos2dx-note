> create at 2018-06-29


# 本周周报

- `cocos2d-x`源码阅读，侧重`network`
- 第三方库的学习，`libcurl`/`libwebsockets`
- 了解`cocos2d-x-3rd-party-libs-src`编译流程
- 尝试复现[#18026](https://github.com/cocos2d/cocos2d-x/issues/18026), [#18022](https://github.com/cocos2d/cocos2d-x/issues/18022), [#17315](https://github.com/cocos2d/cocos2d-x/issues/17315)

# 下周计划

- 升级`websockets`到2.2
- 继续处理网络相关issues