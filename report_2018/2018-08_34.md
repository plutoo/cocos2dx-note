> create at 2018-08-24

# 本周工作
- 剩余平台libwebsockets升级
  - libwebsockets升级到v2.4.2(without libuv)
  - libuv编译集成
  - libwebsocket重编译(with libuv)
- 更新`cocos new`项目模版
  - ios/mac/linux 已完成
  - windows 还有问题
  - android 待处理

# 下周计划
- windows项目模版更新
- downloader/audio相关的多线程处理
- 相关issue处理

