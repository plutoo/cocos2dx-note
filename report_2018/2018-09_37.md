> create at 2018-09-14

# 本周工作
- audio相关代码阅读
- FileUtils增加锁
- FileUtilsAndroid添加listFiles接口
- Data copy改进
- 修复libfmod软链接问题
- [PhysisBody&Follow Bug分析 #10511](https://github.com/cocos2d/cocos2d-x/issues/10511) 

# 下周计划
- PhysisBody&Follow Bug #10511
- 3.18相关ISSUE处理
- 游戏多线程资料收集（Unity相关）